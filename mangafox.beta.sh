#!/bin/bash

#	Manga Pocket, Download any manga from Mangafox to your local drive
#	Copyright (C) 2012  <Sentra Zeheater> Zeheater@yahoo.com
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.


#set -x 	#enable debug-mode
set +x		#disable debug-mode

#Usage: mangafox.sh <option> <url> <name>
#		option:		-s		-single		Download a single chapter
#
#					-m		-multiple	Download multiple chapters
#										Usage: mangafox.sh -m <url> <start> <end> <name>
#
#					-idm	-DownMan	Internet Download Manager extension
#										Usage: mangafox.sh -idm <save_dir> <-s/-m> <url> <start> <end>
#
#					-ls		-list		List all available chapter in orderly numbered fashion

function download_single ( ) { # url, -idm, directory
	let COUNT=1
	
	volume=$(echo $1 | cut -d/ -f6)
	chapter=$(echo $1 | cut -d/ -f7)
	
	payload=$(curl -s --max-time 8 --compressed "$1")
	targetname=$( echo "$payload" | grep "og:description" | cut -d\" -f4)
	
	image=$(echo "$payload" | grep "onerror=\""| cut -d\" -f2)
	echo $image
	
	if [ "$2" != '-idm' ]; then
		until [ -s "$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg" ]; do
			curl -s --compressed $image -o"$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg"
		done
		fsize=$(ls -s -l "$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg" | cut -d' ' -f1 | grep -E ^\-?[0-9]?\.?[0-9]+$)
		until [ $fsize -ge 2 ]; do
			until [ -s "$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg" ]; do
				curl -s --compressed $image -o"$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg"
			done
			fsize=$(ls -s -l "$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg" | cut -d' ' -f1 | grep -E ^\-?[0-9]?\.?[0-9]+$)
		done
	else
		$(idman //n //d "$image" //p "$3" //f $(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg //a)
	fi

	basepage=$(echo "$payload" | grep "id=\"comments\" class=\"r\"" | cut -d\" -f2)
	page=$(echo "$payload" |  grep "onclick=\"return enlarge()\"" | cut -d\" -f4)
	
	until [ "$page" == "javascript:void(0);" -o "$image" == "" ]; do
		nextpage=$(echo "$basepage$page")
		payload=$(curl -s --max-time 8 --compressed "$nextpage")
		if [ "$payload" != "" ]; then
			image=$(echo "$payload" | grep "onerror=\""| cut -d\" -f2)
			echo $image
			COUNT=$((COUNT+1))
			if [ "$2" != '-idm' ]; then
				if [ "$image" != "" ]; then
					until [ -s "$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg" ]; do
						curl -s --compressed $image -o"$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg"
					done
					fsize=$(ls -s -l "$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg" | cut -d' ' -f1 | grep -E ^\-?[0-9]?\.?[0-9]+$)
					until [ $fsize -ge 2 ]; do
						until [ -s "$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg" ]; do
							curl -s --compressed $image -o"$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg"
						done
						fsize=$(ls -s -l "$(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg" | cut -d' ' -f1 | grep -E ^\-?[0-9]?\.?[0-9]+$)
					done
				fi
			else
				$(idman //n //d "$image" //p "$3" //f $(printf '%s-%s-%03d' $volume $chapter $COUNT).jpg //a)
			fi
			basepage=$(echo "$payload" | grep "id=\"comments\" class=\"r\"" | cut -d\" -f2)
			page=$(echo "$payload" |  grep "onclick=\"return enlarge()\"" | cut -d\" -f4)
		fi
	done
}

function download_multi ( ) { #url, start, end
	payload=$(curl -s --compressed "$1")
	let chapternum=1
	echo "$payload" | \
	grep -o -ehttp://mangafox\\.me.*html | \
	cut -d= -f2 | \
	sort -d | \
	while read pagelist; do
		#echo $pagelist
		if [ "$chapternum" -ge "$2" -a "$chapternum" -le "$3" ]; then
				download_single $pagelist $4 $5
		fi
		chapternum=$((chapternum+1))
		if [ "$chapternum" -gt "$3" ]; then
			break
		fi
	done
}

function check7z ( ) {
	exist=$(7z | grep "7-Zip")
	if [ "$exist" != "" ]; then
		echo 'true'
	else
		echo 'false'
	fi
}

function checkIDMan ( ) {
	exist=$(where idman | grep IDMan)
	if [ "$exist" != "" ]; then
		echo 'true'
	else 
		echo 'false'
	fi
}

##-= Main Program =-##
if [ "$1" == '-s' -a $# -ge 3 ]; then
	download_single "$2"
		mkdir "$3"
		mv *.jpg "./$3"
		PATH="$PATH:/d/Program Files/7-Zip/"
		condition=$(check7z)
		if [ "$condition" == "true" ]; then
			7z a -tzip "$3.zip" "$3" >> /dev/null
			rm -r "$3"
		fi
elif [ "$1" == '-m' -a $# -ge 4 ]; then
	download_multi "$2" "$3" "$4"
		#mkdir "$5"
		#mv *.jpg "./$5"
		#PATH="$PATH:/d/Program Files/7-Zip/"
		#condition=$(check7z)
		#if [ "$condition" == "true" ]; then
		#	7z a -tzip "$5.zip" "$5" >> /dev/null
		#	rm -r "$5"
		#fi
elif [ "$1" == '-idm' ]; then
	PATH="${PATH}:/c/Program Files/Internet Download Manager/"
	val=$(checkIDMan)
	if [ "$val" == "false" ]; then
		echo "Internet Download Manager not found"
		exit
	fi
	
	if [ "$3" == '-s' -a $# -ge 4 ]; then
		download_single "$4" '-idm' "$2"
		idman //s
	elif [ "$3" == '-m' -a $# -ge 6 ]; then
		download_multi "$4" "$5" "$6" '-idm' "$2"
		idman //s
	else
		printf 'Wrong or Missing Parameters'
	fi
elif [ "$1" == "-ls"  -a $# -eq 2 ]; then
	let COUNT=1
	payload=$(curl -s --compressed "$2")
	printf 'Chapter\tLinks\r\n'
	echo "$payload" | \
	grep -o -ehttp://mangafox\\.me.*html | \
	cut -d= -f2 | \
	sort -d | \
	while read pagelist; do
		printf '%d\t%s\r\n' $COUNT $pagelist
		COUNT=$((COUNT+1))
	done
else
	printf 'Usage:\tmangafox.sh <option> <url> <name>\r\n'
	printf '\toption:\t-s\t-single\t\tDownload a single chapter\r\n\r\n'
	printf '\t\t-m\t-multiple\tDownload multiple chapters\r\n'
	printf '\t\t\tUse: mangafox.sh -m <url> <start> <end> <name>\r\n\r\n'
	printf '\t\t-idm\t-DownMan\tInternet Download Manager extension\r\n'
	printf '\t\t\tUse: mangafox.sh -idm <save_dir> <-s/-m> <url> <st> <ed>\r\n'
	printf '\t\t-ls\t-list\tList all available chapters\r\n'
	printf '\t\t\tUse: mangafox.sh -ls <url>\r\n'
fi
##-= End Of Main Program =-##