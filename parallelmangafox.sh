#!/bin/bash

#	Manga Pocket, Download any manga from Mangafox to your local drive
#	Copyright (C) 2012  <Sentra Zeheater> Zeheater@yahoo.com
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.


#set -x 	#enable debug-mode
set +x		#disable debug-mode

function check7z ( ) {
	exist=$(7z | grep "7-Zip")
	if [ "$exist" != "" ]; then
		echo 'true'
	else
		echo 'false'
	fi
}

let chapterperthread=$5
if [ $(( ($3-$2+1) % $5 )) -ne 0 ]; then
	let loop=($3-$2+1)/$5+1
else
	let loop=($3-$2+1)/$5
fi
let startbatch=$2
let endbatch=$startbatch+$chapterperthread-1
for (( i=0; i<loop; i++ ))
do
	#echo "download_multi "$1" "$startbatch" "$endbatch"" &
	mangafox.beta.sh -m "$1" "$startbatch" "$endbatch" &
	let startbatch=$endbatch+1
	if [ $(($startbatch+$chapterperthread)) -gt $3 ]; then
		let endbatch=$3
	else 
		let endbatch=$startbatch+$chapterperthread-1
	fi
done

for job in `jobs -p`
do
echo $job
    wait $job || let "FAIL+=1"
done

echo $FAIL

mkdir "$4"
mv *.jpg "./$4"
PATH="$PATH:/d/Program Files/7-Zip/"
condition=$(check7z)
if [ "$condition" == "true" ]; then
	7z a -tzip "$4.zip" "$4" >> /dev/null
	rm -r "$4"
fi
	